const readline = require("readline");
const fs = require("fs");
const file = "entrada.txt";

let text = readline.createInterface({
    input: fs.createReadStream(file)
});

const getLines = () => {
    let cont = 0;
    let scores = [];
    let rounds = 0;
    return new Promise(resolve => {
        text.on("line", linea => {
            if(cont === 0 ) {
                rounds = parseInt(linea);
            }else {
                scores = [...scores, linea.split(' ').map( Number )];
            }
            
            if( cont === rounds ) resolve( scores );
            cont++;
        });
       
      });
}

const work = async () => {
    let result = [ 0, 0 ];
    let leader = [ 0, 0 ];
    const scores = await getLines();
    scores.map( round_score => {
        result = ( round_score[0] > round_score[1] ) ? [1, round_score[0] - round_score[1]] : [2, round_score[1] - round_score[0]];
        if( result[1] > leader[1] ){
            leader = [...result];
        }
    });
}

work();
