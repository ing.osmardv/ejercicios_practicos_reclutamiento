const readline = require("readline");
const fs = require("fs");
const file = "texto.txt";

let text = readline.createInterface({
    input: fs.createReadStream(file)
});

let lenghts;
let first_instruction;
let second_instruction;
let message;

const getLines = () => {
    let cont = 0;
    return new Promise(resolve => {
        text.on("line", linea => {
            if( cont === 0 ) lenghts = linea.split(' ');
            if( cont === 1 ) first_instruction = linea;
            if( cont === 2 ) second_instruction = linea;
            if( cont === 3 ) {
                message = linea;
                resolve()
            };
            cont++;
        });
      });

}

const printLines = async () => {
    await getLines();
    let clean_message = '';
    
    for( let x = 0; x < parseInt(lenghts[2]); x++  ){
        if( clean_message[clean_message.length - 1] !== message[x] ){
            console.log(message[x]);
            clean_message += message[x];
        }
    }
    
    ( clean_message.includes(first_instruction) ) ? console.log('SI') : console.log('NO');
    ( clean_message.includes(second_instruction) ) ? console.log('SI') : console.log('NO');
    
}

printLines();